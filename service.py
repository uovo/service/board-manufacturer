from unicodedata import name
from flask import Flask
from flask_alembic import Alembic
from flask_sqlalchemy import SQLAlchemy
from nameko.rpc import rpc

import requests


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)


class BoardManufacturerService:
    name = "board_manufacturer"

    @rpc
    def manager(self, name):
        r = requests.get('http://192.168.50.206/L/?8*')
        r.status_code
        return "Hello, {}!".format(name)


class BoardManufacturer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    manufacturer = db.Column(db.String(80), unique=True, nullable=False)

    def __repr__(self) -> str:
        return '<Manufacturer %r>' % self.manufacturer


@app.route("/")
def manager():
    return "<p>Hello, World!</p>"


alembic = Alembic()
alembic.init_app(app)
